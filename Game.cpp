#include "Game.h"

#include "StateSplashScreen.h"
#include "StateMainMenu.h"
#include "StateMap.h"
#include "StateCombat.h"
#include "StateCredits.h"
#include "StateGameOver.h"
#include "StateCharacterSelect.h"

#include <ctime>

// pause function to pause for i seconds
void Game::pause(int i) {
	std::clock_t now = std::clock();
	std::clock_t end;
	end = now + i * CLOCKS_PER_SEC;
	while (now < end )
		now = std::clock();
}

// We should be able to detect when errors occur with SDL if there are
// unrecoverable errors, then we need to print an error message and quit the program
// Note this is NOT a class member
void Game::exitFatalError(char *message)
{
    std::cout << message << " " << SDL_GetError();
    SDL_Quit();
    exit(1);
}

// Static data member of class
int Game::instances = 0;

Game::Game(void)
{
	// We should only have ONE instance of the game class
	// Any more than that, and something has gone wrong somewhere!
	instances++;
	if (instances > 1)
		exitFatalError("Attempt to create multiple game instances");
	setupRC();
}

Game::~Game()
{
	
    SDL_DestroyWindow(window);
	SDL_Quit();
}

// Set up rendering context
// Sets values for, and creates an OpenGL context for use with SDL
void Game::setupRC(void)
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        exitFatalError("Unable to initialize SDL");

    // Request an OpenGL 2.1 context.
	// If you request a context not supported by your drivers, no OpenGL context will be created
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1); // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering

	// Optional: Turn on x4 multisampling anti-aliasing (MSAA)
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
 
    // Create 800x600 window
    window = SDL_CreateWindow("SDL OpenGL Demo for GED",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
	
}

// Initialise OpenGL values and game related values and variables
void Game::init(void)
{
	// create and initialise the player pointer
	player = new Player();

	// create and initialise the monster factory
	monsterFactory = new MonsterFactory();

	// initialise the monster pointers
	fodder1 = NULL;
	fodder2 = NULL;
	fodder3 = NULL;
	fodder4 = NULL;
	fodder5 = NULL;

	brute1 = NULL;
	brute2 = NULL;
	brute3 = NULL;

	raider1 = NULL;

	// initialise the item pointers
	healthPack1 = NULL;
	healthPack2 = NULL;

	combatPack1 = NULL;
	combatPack2 = NULL;

	stimulant1 = NULL;

	droppedItem = NULL;

	// create and initialise the states

	characterSelectState = new StateCharacterSelect(this);

	mapState = new StateMap(this);
	mapState->init(*this);

	combatState = new StateCombat(this);
	
	mainMenuState = new StateMainMenu(this);
	mainMenuState->init(*this);

	splashScreenState = new StateSplashScreen(this);
	splashScreenState->init(*this);

	creditsState = new StateCredits(this);
	creditsState->init(*this);

	gameOverState = new StateGameOver(this);
	gameOverState->init(*this);
	
	currentState = splashScreenState;

	glClearColor(0.0, 0.0, 0.0, 0.0); // set background colour

	std::srand( std::time(NULL) );
}

// This function contains the main game loop
void Game::run(void) 
{
	bool running = true; // set running to true
	SDL_Event sdlEvent; // variable to detect SDL events

	std::cout << "Progress: About to enter main loop" << std::endl;

	// unlike GLUT, SDL requires you to write your own event loop
	// This puts much more power in the hands of the programmer
	// This simple loop only responds to the window being closed.
	while (running)	// the event loop
	{
		while (SDL_PollEvent(&sdlEvent))
		{
			if (sdlEvent.type == SDL_QUIT)
				running = false;
			else
				currentState->handleSDLEvent(sdlEvent, *this);

		}
		currentState->update(*this);
		currentState->draw(window, *this);
	}

}

// set current game state
void Game::setState(GameState * newState)
{
	currentState = newState;
}

// get game states
GameState * Game::getState(void)
{
	return currentState;
}

GameState * Game::getCombatState(void)
{
	std::cout << "CombatState" << std::endl;
	combatState->init(*this);
	return combatState;
}

GameState * Game::getMapState(void)
{
	std::cout << "MapState" << std::endl;
	return mapState;
}

GameState * Game::getMainMenuState(void)
{
	std::cout << "MaianMenuState" << std::endl;
	return mainMenuState;
}

GameState * Game::getSplashScreenState(void)
{
	std::cout << "SplashScreenState" << std::endl;
	splashScreenState->init(*this);
	return splashScreenState;
}

GameState * Game::getCreditsState(void)
{
	std::cout << "CreditState" << std::endl;
	creditsState->init(*this);
	return creditsState;
}

GameState * Game::getGameOverState(void)
{
	std::cout << "CreditState" << std::endl;
	return gameOverState;
}

GameState * Game::getCharacterSelectState(void)
{
	std::cout << "CharacterSelectState" << std::endl;
	characterSelectState->init(*this);
	return characterSelectState;
}