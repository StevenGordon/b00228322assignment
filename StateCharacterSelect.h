#ifndef STATECHARACTERSELECT_H
#define STATECHARACTERSELECT_H

#include "GameState.h"
#include "Label.h"

class StateCharacterSelect : public GameState
{
public:
	StateCharacterSelect(Game * gameRefs);
	~StateCharacterSelect(void);

	void init(Game &context);

	void draw(SDL_Window * window, Game &context);

	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context);
	void update(Game &context);

private:
	float characterSelectXPos, characterSelectYPos;

	SDL_Color characterSelectColour;

	Game * gameRef;
	Label * characterSelectLabel;
};

#endif