#ifndef ITEMDECORATOR_H
#define ITEMDECORATOR_H

#include "AbstactItem.h"
#include "Player.h"

class ItemDecorator: public AbstractItem
{
private:
	AbstractItem * item;

public:
	ItemDecorator(AbstractItem *n) { item = n; }

	void draw() { item->draw(); }

	void init() { item->init(); }

	// used to detect collisions with player
	void collision(Player * player) { item->collision(player); }

	/* get and set methods for items */
	// get items stats
	int getHitPoints() { return item->getHitPoints(); }
	int getStrength() { return item->getStrength(); }
	int getSpeed() { return item->getSpeed(); }

	// get and set item position
	float getItemXPos() { return item->getItemXPos(); }
	void setItemXPos(float newXPos) { item->setItemXPos(newXPos); }
	float getItemYPos() { return item->getItemYPos(); }
	void setItemYPos(float newXPos) { item->setItemYPos(newXPos); }
	// get item size
	float getItemXSize() { return item->getItemXSize(); }
	float getItemYSize() { return item->getItemYSize(); }

	// get and set for collided
	// used in states to check if object has collided
	// so the state can be changed
	bool getCollided() { return item->getCollided(); }
	void setCollided(bool ifCollided) { item->setCollided(ifCollided); }

	// get for items draw colours
	GLfloat getRed() { return item->getRed(); }
	GLfloat getGreen() { return item->getGreen(); }
	GLfloat getBlue() { return item->getBlue(); }

	// items effect on player
	void itemEffect(Player * player) { item->itemEffect(player); }

	~ItemDecorator() {delete item;}

protected:
	int health;
	int strength;
	int speed;
};

#endif