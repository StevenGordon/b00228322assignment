#ifndef ITEMHEALTHPACK_H
#define ITEMHEALTHPACK_H

class ItemHealthPack: public ItemDecorator
{
public:
	ItemHealthPack(AbstractItem *n): ItemDecorator(n) { }

	void draw()
	{
		glColor3f(red, green, blue);
		glBegin(GL_POLYGON);
			glVertex3f (itemXPos, itemYPos, 0.0); // first corner
			glVertex3f (itemXPos+itemXSize, itemYPos, 0.0); // second corner
			glVertex3f (itemXPos+itemXSize, itemYPos+itemYSize, 0.0); // third corner
			glVertex3f (itemXPos, itemYPos+itemYSize, 0.0); // fourth corner
		glEnd();

		ItemDecorator::draw();
	}

	void init()
	{
		red = getRed(); green = getGreen(); blue = getBlue();

		hitPoints = getHitpoints();
		strength = getStrength();
		speed = getSpeed();

		itemXPos = (float)( (rand() %170)/100.0f - 0.9f );
		itemYPos = (float)( (rand() %170)/100.0f - 0.9f );
		itemXSize = 0.05f;
		itemYSize = 0.05f;
	}

	// used to detect collisions with player
	void collision(Player * player)
	{
		if ( (itemXPos >= player->getPlayerXPos())
			&& (itemXPos+itemXSize <= player->getPlayerXPos()+player->getPlayerXSize())		// cursor surrounds target in x
			&& (itemYPos >= player->getPlayerYPos())
			&& (itemYPos+itemYSize <= player->getPlayerYPos()+player->getPlayerXSize()) )	// cursor surrounds target in y
			setCollided(true);	// item is surrounded by player in both X and Y coords
		else
			setCollided(false);	// item is not surrounded by player in both X and Y coords

		ItemDecorator::collision(player);
	}

	/* get and set methods for items */
	// get items stats
	int getHitpoints() { return ItemDecorator::getHitPoints(); }
	int getStrength() {	return ItemDecorator::getStrength(); }
	int getSpeed() { return ItemDecorator::getSpeed(); }

	// get and set item position
	float getItemXPos() { return ItemDecorator::getItemXPos(); }
	void setItemXPos(float newXPos) { ItemDecorator::setItemXPos(newXPos); }
	float getItemYPos() { return ItemDecorator::getItemYPos(); }
	void setItemYPos(float newXPos) { ItemDecorator::setItemYPos(newXPos); }
	// get item size
	float getItemXSize() { return ItemDecorator::getItemXSize(); }
	float getItemYSize() { return ItemDecorator::getItemYSize(); }

	// get and set for collided
	// used in states to check if object has collided
	// so the state can be changed
	bool getCollided() { return ItemDecorator::getCollided(); }
	void setCollided(bool ifCollided) { ItemDecorator::setCollided(ifCollided); }

	// get for items draw colours
	GLfloat getRed() { return ItemDecorator::getRed(); }
	GLfloat getGreen() { return ItemDecorator::getGreen(); }
	GLfloat getBlue() { return ItemDecorator::getBlue(); }

	// items effect on player
	void itemEffect(Player * player)
	{
		// as this item has two effects
		// check to see which effect should be used is performed

		// if the players current HP are not equal to
		// the players total HP
		if( player->getPlayerCurrentHitPoints() != player->getPlayerTotalHitPoints() )
		{
		// the item sets the players current HP to teh total HP
			player->setPlayerCurrentHitPoints( player->getPlayerTotalHitPoints() );
		}
		else 
		{
		// player current HP is equal to total HP
		//set total HP to equal total HP + item HP
			player->setPlayerTotalHitPoints( player->getPlayerTotalHitPoints()
				+ getHitPoints() );
		// set current HP to equal total HP
			player->setPlayerCurrentHitPoints( player->getPlayerTotalHitPoints() );
		}

		ItemDecorator::itemEffect(player);
	}

	
	~ItemHealthPack() { std::cout << "Deleting Health Pack decorator" << std::endl; }
};

#endif