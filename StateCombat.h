#ifndef STATECOMBAT_H
#define STATECOMBAT_H

#include "GameState.h"
#include "StateMap.h"

#include "Label.h"
#include "Player.h"
#include "Monster.h"

#include <cstdlib>
#include <ctime>

class StateCombat : public GameState
{
public:
	StateCombat(Game * gameRefs);
	~StateCombat(void);

	void init(Game &context);

	Monster * getMonster();

	void draw(SDL_Window * window, Game &context);

	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context);
	void update(Game &context);

	void setCoords();

	void combat(Game &context);

	void speedCheck();
	void fight();
	void itemDrop(Game &context);
	void checkDefeated(Game &context);
	void endFight();
	
private:

	bool fightCompleted;

	int score;

	int orderOfAttack;		// 1 = player attacks first 2 = monster attacks first

	int playerInitialHitPoints;

	float tempPlayerX, tempPlayerY;
	float playerXPos, playerYPos;

	float tempMonsterX, tempMonsterY;
	float monsterXPos, monsterYPos;

	Game * gameRef;

	Label * playerStatsLabel;
	Label * monsterStatsLabel;
};

#endif
