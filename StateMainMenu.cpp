#include "StateMainMenu.h"
#include <iostream>

// While a declaration of class Game has been provided in
// gamestate.h, this was only a forward declaration
// because we need to work with the implementation now
// we need to include game.h here
#include "Game.h"

StateMainMenu::StateMainMenu(Game * gameRefs)
{
	gameRef = gameRefs;
}

StateMainMenu::~StateMainMenu(void)
{
}

void StateMainMenu::init(Game &context)
{
	titleXPos = -0.9f; titleYPos = 0.7f;
	menuXPos = -0.5f; menuYPos = 0.5f;
	newGameXPos = -0.4f; newGameYPos = 0.1f;
	continueGameXPos = -0.4f; continueGameYPos = -0.1f;
	creditsXPos = -0.4f; creditsYPos = -0.3f;
	quitGameXPos = -0.4f; quitGameYPos = -0.5f;

	titleLabel = new Label();
	menuLabel = new Label();
	newGame = new Label();
	continueGame = new Label();
	credits = new Label(),
	quitGame = new Label();
}

void StateMainMenu::draw(SDL_Window * window, Game &context)
{
	// sets label colour
	SDL_Color labelColour = { 100, 70, 255 };

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	// draw Main Menu
	titleLabel->displayString(titleXPos, titleYPos, "Galactic Marines", labelColour, 0.01f);

	// draw Main Menu
	menuLabel->displayString(menuXPos, menuYPos, "Main Menu", labelColour, 0.004f);

	// draw New Game
	newGame->displayString(newGameXPos, newGameYPos, "[n] - New Game", labelColour, 0.004f);

	// draw Continue
	continueGame->displayString(continueGameXPos, continueGameYPos, "[c] - Continue", labelColour, 0.004f);

	// draw Credits
	credits->displayString(creditsXPos, creditsYPos, "[r] - Credits", labelColour, 0.004f);

	// draw Quit
	quitGame->displayString(quitGameXPos, quitGameYPos, "[ESC/q] - Quit", labelColour, 0.004f);

	SDL_GL_SwapWindow(window); // swap buffers
}

// switches state to character select state by pressing the [n] key
// switches state to credits state by pressing the [r] key
// switches state to map state by pressing the [c]  key
// exited the program by pressing the [q] or ESC key
void StateMainMenu::handleSDLEvent(SDL_Event const &sdlEvent, Game &context)
{
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		//std::cout << "Scancode: " << sdlEvent.key.keysym.scancode  ;
        //std::cout <<  ", Name: " << SDL_GetKeyName( sdlEvent.key.keysym.sym ) << std::endl;
		switch( sdlEvent.key.keysym.sym )
		{
		case SDLK_c:
			context.setState(context.getMapState());
			break;
			/*
		case SDLK_s:											// Test to check if the timer
			context.setState(context.getSplashScreenState());	// worked for both the credits
			break;												// and the splash screen
			*/
		case SDLK_n:
			context.setState(context.getCharacterSelectState());
			break;
		case SDLK_r:
			context.setState(context.getCreditsState());
			break;
		case SDLK_q:
		case SDLK_ESCAPE:
			// Create a SDL quit event and push into the event queue
			SDL_Event quitEvent;
			quitEvent.type = SDL_QUIT;
			SDL_PushEvent(&quitEvent);
		default:
			break;
		}
	}
}

void StateMainMenu::update(Game &context) { }