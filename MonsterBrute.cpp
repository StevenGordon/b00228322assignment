#include "MonsterBrute.h"

MonsterBrute::MonsterBrute(void) { }

MonsterBrute::~MonsterBrute(void) { }

// sets monster variables
void MonsterBrute::init()
{
	hasCollided = false;

	monsterHitPoints = 12;
	monsterStrength = 8;
	monsterSpeed = 6;
	monsterDollars = 10;
	monsterItemChance = 15;

	monsterXPos = (float)( (rand() %170)/100.0f - 0.9f );
	monsterYPos = (float)( (rand() %170)/100.0f - 0.9f );
	monsterXSize = 0.1f;
	monsterYSize = 0.1f;
}

void MonsterBrute::draw(SDL_Window * window)
{
	// set label colour
	SDL_Color colour = { 255, 255, 0 };

	glColor3f(1.0, 1.0, 0.0);
	glBegin(GL_POLYGON);
		glVertex3f (monsterXPos, monsterYPos, 0.0); // first corner
		glVertex3f (monsterXPos+monsterXSize, monsterYPos, 0.0); // second corner
		glVertex3f (monsterXPos+monsterXSize, monsterYPos+monsterYSize, 0.0); // third corner
		glVertex3f (monsterXPos, monsterYPos+monsterYSize, 0.0); // fourth corner
	glEnd();

	// draw monster label
	displayLabel(monsterXPos + monsterXSize/2.0f, monsterYPos + monsterYSize, "Brute", colour, 0.002f);
}

// gives the player dollars and
// checks if the monster drops an item
bool MonsterBrute::drop(Player * player)
{
	player->setPlayerDollars(monsterDollars);

	if( (float)( rand() %100 ) <= monsterItemChance )
		return true;
	else return false;
}