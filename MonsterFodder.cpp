#include "MonsterFodder.h"

MonsterFodder::MonsterFodder(void) { }

MonsterFodder::~MonsterFodder(void) { }

// sets monster variables
void MonsterFodder::init()
{
	hasCollided = false;

	monsterHitPoints = 3;
	monsterStrength = 3;
	monsterSpeed = 7;
	monsterDollars = 1;
	monsterItemChance = 5;

	monsterXPos = (float)( (rand() %170)/100.0f - 0.9f );
	monsterYPos = (float)( (rand() %170)/100.0f - 0.9f );
	monsterXSize = 0.1f;
	monsterYSize = 0.1f;
}

void MonsterFodder::draw(SDL_Window * window)
{
	// set label colour
	SDL_Color colour = { 255, 255, 0 };

	glColor3f(0.0, 1.0, 0.0);
	glBegin(GL_POLYGON);
		glVertex3f (monsterXPos, monsterYPos, 0.0); // first corner
		glVertex3f (monsterXPos+monsterXSize, monsterYPos, 0.0); // second corner
		glVertex3f (monsterXPos+monsterXSize, monsterYPos+monsterYSize, 0.0); // third corner
		glVertex3f (monsterXPos, monsterYPos+monsterYSize, 0.0); // fourth corner
	glEnd();

	// draw monster label
	displayLabel(monsterXPos + monsterXSize/2.0f, monsterYPos + monsterYSize, "Fodder", colour, 0.002f);
}

// gives the player dollars and
// checks if the monster drops an item
bool MonsterFodder::drop(Player * player)
{
	player->setPlayerDollars(monsterDollars);
	int dropChance = (float)( rand() %100 );
	std::cout << "drop " << dropChance << std::endl;
	if( dropChance <= monsterItemChance )
		return true;
	else return false;
}