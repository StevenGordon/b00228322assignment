#ifndef ITEM_H
#define ITEM_H

class Item: public AbstractItem
{
private:
	std::string name;

protected: 
	float itemXPos, itemYPos;
	float itemXSize, itemYSize;

public:
	// constructors all the items effect variables and colour are added here
	// collided boolean is also set to false here
	Item(std::string basename, int hp, int str, int spd, GLfloat ired, GLfloat igreen, GLfloat iblue)
	{ name.assign(basename); hitPoints = hp; strength = str; speed = spd; red = ired; green = igreen; blue = iblue; collided = false; } // assign is a string function
	Item(char * basename, int hp, int str, int spd, GLfloat ired, GLfloat igreen, GLfloat iblue)
	{ name.assign(basename); hitPoints = hp; strength = str; speed = spd; red = ired; green = igreen; blue = iblue; collided = false; } // to set string contents

	void draw()	{ }

	void init() { }

	// used to detect collisions with player
	void collision(Player * player)	{ }

	/* get and set methods for items */
	// get items stats
	int getHitPoints() { return hitPoints; }
	int getStrength() { return strength; }
	int getSpeed() { return speed; }

	// get and set item position
	float getItemXPos() { return itemXPos; }
	void setItemXPos(float newXPos) { itemXPos = newXPos; }
	float getItemYPos() { return itemYPos; }
	void setItemYPos(float newXPos) { itemYPos = newXPos; }
	// get item size
	float getItemXSize() { return itemXSize; }
	float getItemYSize() { return itemYSize; }

	// get and set for collided
	// used in states to check if object has collided
	// so the state can be changed
	bool getCollided() { return collided; }
	void setCollided(bool ifCollided) { collided = ifCollided; }

	// get for items draw colours
	GLfloat getRed() { return red; }
	GLfloat getGreen() { return green; }
	GLfloat getBlue() { return blue; }

	// items effect on player
	void itemEffect(Player * player) { }

	~Item() { std::cout << "Deleting Item object " << name << std::endl; }
};

#endif
