#include "MonsterFactory.h"

Monster * MonsterFactory::orderMonster(std::string type)
{
	Monster * newMonster;
	newMonster = createMonster(type);

	return newMonster;
}

Monster * MonsterFactory::createMonster(std::string type)
{
	if (type == "fodder") return new MonsterFodder();
	else if(type == "brute") return new MonsterBrute();
	else if(type == "raider") return new MonsterRaider();

	return NULL;
}