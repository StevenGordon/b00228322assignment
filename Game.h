#ifndef GAME_H
#define GAME_H

#include <GL/glew.h>
#include <SDL.h>
#include <SDL_ttf.h>

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <sstream>
#include <string>

#include "GameState.h"

#include "label.h"

#include "AbstactItem.h"
#include "Item.h"
#include "ItemDecorator.h"
#include "ItemCombatPack.h"
#include "ItemHealthPack.h"
#include "ItemStimulant.h"

#include "Player.h"

#include "Monster.h"
#include "MonsterFactory.h"
#include "MonsterFodder.h"
#include "MonsterBrute.h"
#include "MonsterRaider.h"

class Game{

public:
	// Constructor and destructor methods
	Game(void);
	~Game();

	// init and run are the only functions that need to be called from main
	void init(void);
	void run(void);
	
	TTF_Font * getFont(void) { return textFont; }

	// setState to change states
	void setState(GameState * newState);

	// getStates to return a state
	GameState *getState(void);
	GameState *getCombatState(void);
	GameState *getMapState(void);
	GameState *getMainMenuState(void);
	GameState *getSplashScreenState(void);
	GameState *getCreditsState(void);
	GameState *getGameOverState(void);
	GameState *getCharacterSelectState(void);

	// get function for player
	Player * getPlayer() { return player; }

	/* get and set functions for monster */
	// set and get for fodder monsters
	Monster * getFodder1() { return fodder1; }
	void setFodder1(Monster * monster) { fodder1 = monster; }

	Monster * getFodder2() { return fodder2; }
	void setFodder2(Monster * monster) { fodder2 = monster; }

	Monster * getFodder3() { return fodder3; }
	void setFodder3(Monster * monster) { fodder3 = monster; }

	Monster * getFodder4() { return fodder4; }
	void setFodder4(Monster * monster) { fodder4 = monster; }

	Monster * getFodder5() { return fodder5; }
	void setFodder5(Monster * monster) { fodder5 = monster; }

	// set and get for brute monsters
	Monster * getBrute1() { return brute1; }
	void setBrute1(Monster * monster) { brute1 = monster; }

	Monster * getBrute2() { return brute2; }
	void setBrute2(Monster * monster) { brute2 = monster; }

	Monster * getBrute3() { return brute3; }
	void setBrute3(Monster * monster) { brute3 = monster; }

	// set and get for raider monsters
	Monster * getRaider1() { return raider1; }
	void setRaider1(Monster * monster) { raider1 = monster; }

	// get for monster factory
	MonsterFactory * getMonsterFactory() { return monsterFactory; }

	/* get and set for items */
	// get and set for health pack items
	AbstractItem * getHealthPack1() { return healthPack1; }
	void setHealthPack1(AbstractItem * item) { healthPack1 = item; }

	AbstractItem * getHealthPack2() { return healthPack2; }
	void setHealthPack2(AbstractItem * item) { healthPack2 = item; }

	// get and set for combat pack items
	AbstractItem * getCombatPack1() { return combatPack1; }
	void setCombatPack1(AbstractItem * item) { combatPack1 = item; }

	AbstractItem * getCombatPack2() { return combatPack2; }
	void setCombatPack2(AbstractItem * item) { combatPack2 = item; }

	// get and set for stimulant items
	AbstractItem * getStimulant1() { return stimulant1; }
	void setStimulant1(AbstractItem * item) { stimulant1 = item; }

	AbstractItem * getDroppedItem() { return droppedItem; }
	void setDroppedItem(AbstractItem * item) { droppedItem = item; }

	static void exitFatalError(char *message);
	void pause(int i);	

private:
	friend class StateMap;
	friend class StateCombat;

	// The game states:
	GameState * combatState;
	GameState * mapState;
	GameState * mainMenuState;
	GameState * splashScreenState;
	GameState * creditsState;
	GameState * gameOverState;
	GameState * characterSelectState;
	GameState * currentState;

	// The player
	Player * player;

	// The monsters
	// Fodder
	Monster * fodder1;
	Monster * fodder2;
	Monster * fodder3;
	Monster * fodder4;
	Monster * fodder5;
	// Brute
	Monster * brute1;
	Monster * brute2;
	Monster * brute3;
	// Raider
	Monster * raider1;
	
	// The monster factory
	MonsterFactory* monsterFactory;

	// The items
	AbstractItem * healthPack1;
	AbstractItem * healthPack2;

	AbstractItem * combatPack1;
	AbstractItem * combatPack2;

	AbstractItem * stimulant1;

	AbstractItem * droppedItem;

	// setupRC will be called when game is created
	void setupRC(void);
	// The game loop will call update, draw and handle event methods provided by each state

	SDL_Window *window;
	
	SDL_GLContext context;

	static int instances;

	clock_t lastTime; // clock_t is an integer type
	clock_t currentTime; // use this to track time between frames

	TTF_Font * textFont;	// SDL type for True-Type font rendering
};

#endif