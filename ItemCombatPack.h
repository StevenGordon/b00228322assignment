#ifndef ITEMCOMBATPACK_H
#define ITEMCOMBATPACK_H

class ItemCombatPack: public ItemDecorator
{
public:
	ItemCombatPack(AbstractItem *n): ItemDecorator(n) { }

	void draw()
	{		
		glColor3f(red, green, blue);
		glBegin(GL_POLYGON);
			glVertex3f (itemXPos, itemYPos, 0.0); // first corner
			glVertex3f (itemXPos+itemXSize, itemYPos, 0.0); // second corner
			glVertex3f (itemXPos+itemXSize, itemYPos+itemYSize, 0.0); // third corner
			glVertex3f (itemXPos, itemYPos+itemYSize, 0.0); // fourth corner
		glEnd();

		ItemDecorator::draw();
	}

	void init()
	{
		red = getRed(); green = getGreen(); blue = getBlue();

		hitPoints = getHitpoints();
		strength = getStrength();
		speed = getSpeed();

		itemXPos = (float)( (rand() %170)/100.0f - 0.9f );
		itemYPos = (float)( (rand() %170)/100.0f - 0.9f );
		itemXSize = 0.05f;
		itemYSize = 0.05f;
	}

	// used to detect collisions with player
	void collision(Player * player)
	{
		if ( (itemXPos >= player->getPlayerXPos())
			&& (itemXPos+itemXSize <= player->getPlayerXPos()+player->getPlayerXSize())		// cursor surrounds target in x
			&& (itemYPos >= player->getPlayerYPos())
			&& (itemYPos+itemYSize <= player->getPlayerYPos()+player->getPlayerXSize()) )	// cursor surrounds target in y
			setCollided(true);	// item is surrounded by player in both X and Y coords
		else
			setCollided(false);	// item is not surrounded by player in both X and Y coords

		ItemDecorator::collision(player);
	}

	/* get and set methods for items */
	// get items stats
	int getHitpoints() { return ItemDecorator::getHitPoints(); }
	int getStrength() {	return ItemDecorator::getStrength(); }
	int getSpeed() { return ItemDecorator::getSpeed(); }

	// get and set item position
	float getItemXPos() { return ItemDecorator::getItemXPos(); }
	void setItemXPos(float newXPos) { ItemDecorator::setItemXPos(newXPos); }
	float getItemYPos() { return ItemDecorator::getItemYPos(); }
	void setItemYPos(float newXPos) { ItemDecorator::setItemYPos(newXPos); }
	// get item size
	float getItemXSize() { return ItemDecorator::getItemXSize(); }
	float getItemYSize() { return ItemDecorator::getItemYSize(); }

	// get and set for collided
	// used in states to check if object has collided
	// so the state can be changed
	bool getCollided() { return ItemDecorator::getCollided(); }
	void setCollided(bool ifCollided) { ItemDecorator::setCollided(ifCollided); }

	// get for items draw colours
	GLfloat getRed() { return ItemDecorator::getRed(); }
	GLfloat getGreen() { return ItemDecorator::getGreen(); }
	GLfloat getBlue() { return ItemDecorator::getBlue(); }

	// items effect on player
	void itemEffect(Player * player)
	{
		// newStrength is the players new strength stat
		// calculated by the players strength + the items strength
		int newStrength = (player->getPlayerStrength() + getStrength());
		player->setPlayerStrength(newStrength);

		ItemDecorator::itemEffect(player);
	}

	~ItemCombatPack() { std::cout << "Deleting Combat Pack decorator" << std::endl; }
};

#endif