#ifndef MONSTERFACTORY_H
#define MONSTERFACTORY_H

#include "Monster.h"
#include "MonsterFodder.h"
#include "MonsterBrute.h"
#include "MonsterRaider.h"

#include <string>

class MonsterFactory
{
public:
	Monster * orderMonster(std::string type);
	Monster * createMonster(std::string type);
};

#endif