#ifndef PLAYER_H
#define PLAYER_H

#include <GL/glew.h>
#include <SDL.h>
#include <SDL_ttf.h>
#include <iostream>
#include <cstdlib>
#include <ctime>

#include "Label.h"

class Player
{
public:
	Player(void);
	~Player(void);

	void init();

	void draw(SDL_Window * window);

	/* Set and get functions */
	// set and get functions for players X and Y coordinates
	float getPlayerXPos() { return playerXPos; }
	float getPlayerYPos() { return playerYPos; }

	void setPlayerXPos(float newXPos) { playerXPos = newXPos; }
	void setPlayerYPos(float newYPos) { playerYPos = newYPos; }

	float getPlayerXSize() { return playerXSize; }
	float getPlayerYSize() { return playerYSize; }

	void setPlayerXSize(float newXPos) { playerXSize = newXPos; }
	void setPlayerYSize(float newYPos) { playerYSize = newYPos; }

	void movePlayerXPos(float newXPos) { playerXPos += newXPos; }
	void movePlayerYPos(float newYPos) { playerYPos += newYPos; }

	// set and get functions for players total hitpoints (HP)
	int getPlayerTotalHitPoints() { return playerTotalHitPoints; }
	void setPlayerTotalHitPoints(int newHitPoints) { playerTotalHitPoints = newHitPoints; }

	void setDamage(int damage) { playerCurrentHitPoints -= damage; }
	void setheal(int heal) { playerCurrentHitPoints += heal; }

	// set and get functions for players current hitpoints (HP)
	int getPlayerCurrentHitPoints() { return playerCurrentHitPoints; }
	void setPlayerCurrentHitPoints(int newHitPoints) { playerCurrentHitPoints = newHitPoints; }

	// set and get functions for players strength (STR)
	int getPlayerStrength() { return playerStrength; }
	void setPlayerStrength(int newStrength) { playerStrength = newStrength; }

	// set and get functions for players speed (SPD)
	int getPlayerSpeed() { return playerSpeed; }
	void setPlayerSpeed(int newSpeed) { playerSpeed = newSpeed; }

	// set and get functions for players speed ($)
	int getPlayerDollars() { return playerDollars; }
	void setPlayerDollars(int newDollars) { playerDollars += newDollars; }

	// get player damage
	int getDamage();

private:
	// Player Coords
	float playerXPos, playerYPos;
	float playerXSize, playerYSize;

	// Player stats
	int playerTotalHitPoints, playerCurrentHitPoints;
	int playerStrength, playerSpeed;
	int playerDollars;

	// player label
	Label * playerLabel;
};

#endif