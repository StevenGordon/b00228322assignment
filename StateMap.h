#ifndef STATEMAP_H
#define STATEMAP_H

#include "GameState.h"

#include "Label.h"
#include "Player.h"
#include "Monster.h"

// C stdlib and C time libraries for rand and time functions
#include <cstdlib>
#include <ctime>

class Game;

class StateMap : public GameState
{
public:
	StateMap(Game * gameRefs);
	~StateMap(void);

	void init(Game &context);
	void draw(SDL_Window * window, Game &context);

	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context);
	void update(Game &context);

	void itemCollision(Game &context);

	void itemHealthCollision(Game &context);
	void itemCombatCollision(Game &context);
	void itemStimCollision(Game &context);

	void collision(Game &context);

	void resetMonsters(Game &context);
	void monsterdead(Game &context);

private:
	int score;

	int deadMonsterCounter;

	float xpos, ypos;
	float xsize, ysize;

	float targetXPos, targetYPos;
	float targetXSize, targetYSize;

	Game * gameRef;

	Label * playerStatesLabel;

	TTF_Font * textFont;	// SDL type for True-Type font rendering
};

#endif
