#include "Player.h"

Player::Player(void)
{
}

Player::~Player(void)
{
}

// initializes the players values
// to be used when the player is required
// to be drawn on screen for the first time
// in a new game
void Player::init()
{
	playerTotalHitPoints = 10;
	playerCurrentHitPoints = playerTotalHitPoints;
	playerStrength = 10;
	playerSpeed = 10;
	playerXPos = 0.0f;
	playerYPos = 0.0f;
	playerXSize = 0.15f;
	playerYSize = 0.15f;
	playerDollars = 0;

	playerLabel = new Label();
}

void Player::draw(SDL_Window * window)
{
	SDL_Color GameColour = { 255, 255, 0 };

	glColor3f(1.0,1.0,1.0);

	glBegin(GL_POLYGON);
		glVertex3f (playerXPos, playerYPos, 0.0); // first corner
		glVertex3f (playerXPos+playerXSize, playerYPos, 0.0); // second corner
		glVertex3f (playerXPos+playerXSize, playerYPos+playerYSize, 0.0); // third corner
		glVertex3f (playerXPos, playerYPos+playerYSize, 0.0); // fourth corner
	glEnd();

	playerLabel->displayString(playerXPos + (playerXSize/2.0f),	playerYPos + playerYSize, "Player", GameColour, 0.002f);
}

// player damage is  a random number
// betwwen 2 and the players strength
int Player::getDamage()
	{
		srand(time(NULL));
		return 2 + (float)(rand() %(playerStrength-2));
	}