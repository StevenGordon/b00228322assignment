#ifndef STATEGAMEOVER_H
#define STATEGAMEOVER_H

#include "GameState.h"
#include "Label.h"

class StateGameOver : public GameState
{
public:
	StateGameOver(Game * gameRefs);
	~StateGameOver(void);

	void init(Game &context);
	void draw(SDL_Window * window, Game &context);

	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context);
	void update(Game &context);

private:
	float gameOverXPos, gameOverYPos;

	SDL_Color gameOverColour;

	Game * gameRef;
	Label * GameOverLabel;
};

#endif