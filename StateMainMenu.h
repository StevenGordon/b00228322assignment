#ifndef STATE_MAIN_MENU
#define STATE_MAIN_MENU

#include "GameState.h"
#include "Label.h"

class StateMainMenu : public GameState
{
public:
	StateMainMenu(Game * gameRefs);
	~StateMainMenu(void);

	void init(Game &context);
	void draw(SDL_Window * window, Game &context);

	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context);
	void update(Game &context);

private:
	float titleXPos, titleYPos;
	float menuXPos, menuYPos;
	float newGameXPos, newGameYPos;
	float continueGameXPos, continueGameYPos;
	float creditsXPos, creditsYPos;
	float quitGameXPos, quitGameYPos;

	TTF_Font *textFont;	// SDL type for True-Type font rendering

	Game * gameRef;
	Label * titleLabel;
	Label * menuLabel;
	Label * newGame;
	Label * continueGame;
	Label * credits;
	Label * quitGame;
};

#endif
