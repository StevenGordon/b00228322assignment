#ifndef LABEL_H
#define LABEL_H

#include <GL/glew.h>
#include <SDL.h>
#include <SDL_ttf.h>

class Label
{
public:
	Label();
	
	GLuint getTex(void) {return texID; }
	void displayString(float x, float y, const char * str, SDL_Color colour, GLfloat scale);
	GLuint textToTexture(const char * str, SDL_Color colour);
	void draw(GLfloat x, GLfloat y);
	void draw(GLfloat x, GLfloat y, GLfloat scale);

private:
	GLuint width;
	GLuint height;
	GLuint texID;
};

#endif