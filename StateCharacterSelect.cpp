#include "StateCharacterSelect.h"
#include <iostream>

#include "Game.h"


StateCharacterSelect::StateCharacterSelect(Game * gameRefs)
{
	gameRef = gameRefs;
}

StateCharacterSelect::~StateCharacterSelect(void)
{
}

// creates and initializes objects the state requires 
void StateCharacterSelect::init(Game &context)
{
	characterSelectLabel = new Label();
	characterSelectXPos = -0.85f; characterSelectYPos = 0.7f;

	/* checks if monsters are null and if so creates them */
	// checks and creates the fodder monsters
	if(context.getFodder1() == NULL)
		context.setFodder1(context.getMonsterFactory()->orderMonster("fodder"));
	if(context.getFodder2() == NULL)
		context.setFodder2(context.getMonsterFactory()->orderMonster("fodder"));
	if(context.getFodder3() == NULL)
		context.setFodder3(context.getMonsterFactory()->orderMonster("fodder"));
	if(context.getFodder4() == NULL)
		context.setFodder4(context.getMonsterFactory()->orderMonster("fodder"));
	if(context.getFodder5() == NULL)
		context.setFodder5(context.getMonsterFactory()->orderMonster("fodder"));
	// checks and creates the brute monsters
	if(context.getBrute1() == NULL)
		context.setBrute1(context.getMonsterFactory()->orderMonster("brute"));
	if(context.getBrute2() == NULL)
		context.setBrute2(context.getMonsterFactory()->orderMonster("brute"));
	if(context.getBrute3() == NULL)
		context.setBrute3(context.getMonsterFactory()->orderMonster("brute"));
	// checks and creates the raider monsters
	if(context.getRaider1() == NULL)
	context.setRaider1(context.getMonsterFactory()->orderMonster("raider"));

	/* checks if items are null and if so creates them */
	// checks and creates the health pack items
	if(context.getHealthPack1() == NULL)
		context.setHealthPack1(  new ItemHealthPack(new Item("Health Pack", 1, 0, 0, 1.0f, 0.0f, 0.0f)) );
	if(context.getHealthPack2() == NULL)
		context.setHealthPack2(  new ItemHealthPack(new Item("Health Pack", 1, 0, 0, 1.0f, 0.0f, 0.0f)) );
	// checks and creates the combat pack items
	if(context.getCombatPack1() == NULL)
		context.setCombatPack1(  new ItemCombatPack(new Item("Combat Pack", 0, 2, 0, 0.0f, 0.0f, 1.0f)) );
	if(context.getCombatPack2() == NULL)
		context.setCombatPack2(  new ItemCombatPack(new Item("Combat Pack", 0, 2, 0, 0.0f, 0.0f, 1.0f)) );
	// checks and creates the stimulant items
	if(context.getStimulant1() == NULL)
		context.setStimulant1( new ItemStimulant(new Item("Stimulant", 0, 0, 1, 0.0f, 1.0f, 0.0f)) );
}

void StateCharacterSelect::draw(SDL_Window * window, Game &context)
{
	// sets the labels colour
	SDL_Color creditsColour = { 255, 255, 255 };

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window

	// draw title Character select
	characterSelectLabel->displayString(characterSelectXPos, characterSelectYPos, "Character Select", creditsColour, 0.01f);

	SDL_GL_SwapWindow(window); // swap buffers
}

// switches state to mapstate by pressing the space or return key
void StateCharacterSelect::handleSDLEvent(SDL_Event const &sdlEvent, Game &context)
{
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		//std::cout << "Scancode: " << sdlEvent.key.keysym.scancode  ;
        //std::cout <<  ", Name: " << SDL_GetKeyName( sdlEvent.key.keysym.sym ) << std::endl;
		switch( sdlEvent.key.keysym.sym )
		{
		case SDLK_RETURN:
		case SDLK_SPACE:
			context.getMapState()->init(context);
			context.setState(context.getMapState());
		default:
			break;
		}
	}
}

void StateCharacterSelect::update(Game &context)
{
}