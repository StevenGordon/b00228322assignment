#include "StateGameOver.h"
#include <iostream>

#include "Game.h"

StateGameOver::StateGameOver(Game * gameRefs)
{
	gameRef = gameRefs;
}

StateGameOver::~StateGameOver(void)
{
}

void StateGameOver::init(Game &context)
{
	GameOverLabel = new Label();
	gameOverXPos = -0.85f; gameOverYPos = 0.0f;
}

void StateGameOver::draw(SDL_Window * window, Game &context)
{
	// sets label colour
	SDL_Color gameOverColour = { 100, 70, 255 };

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window

	// draw Game Over display
	GameOverLabel->displayString(gameOverXPos, gameOverYPos, "GAME OVER", gameOverColour, 0.01f);

	SDL_GL_SwapWindow(window); // swap buffers
}

// switches state to main menu state by pressing the space or return key
void StateGameOver::handleSDLEvent(SDL_Event const &sdlEvent, Game &context)
{
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		//std::cout << "Scancode: " << sdlEvent.key.keysym.scancode  ;
        //std::cout <<  ", Name: " << SDL_GetKeyName( sdlEvent.key.keysym.sym ) << std::endl;
		switch( sdlEvent.key.keysym.sym )
		{
		case SDLK_RETURN:
		case SDLK_SPACE:
			context.setState(context.getMainMenuState());
		default:
			break;
		}
	}
}

void StateGameOver::update(Game &context)
{
}