#ifndef STATECREDITS_H
#define STATECREDITS_H

#include "GameState.h"
#include "Label.h"
#include <ctime>

class StateCredits : public GameState
{
public:
	StateCredits(Game * gameRefs);
	~StateCredits(void);

	void init(Game &context);

	void draw(SDL_Window * window, Game &context);

	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context);
	void update(Game &context);

private:
	float creditsXPos, creditsYPos;

	int delay;

	clock_t now;
	clock_t end;

	Game * gameRef;
	Label * creditsLabel;
};

#endif