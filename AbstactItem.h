#ifndef ABSTRACTITEM_H
#define ABSTRACTITEM_H

#include "Player.h"

class AbstractItem
{
public:
	virtual void draw() = 0;

	virtual void init() = 0;

	// used to detect collisions with player
	virtual void collision(Player * player) = 0;

	/* get and set methods for items */
	// get items stats
	virtual int getHitPoints() = 0;
	virtual int getStrength() = 0;
	virtual int getSpeed() = 0;

	// get and set item position
	virtual float getItemXPos() = 0;
	virtual void setItemXPos(float newXPos) = 0;
	virtual float getItemYPos() = 0;
	virtual void setItemYPos(float newYPos) = 0;
	// get item size
	virtual float getItemXSize() = 0;
	virtual float getItemYSize() = 0;

	// get and set for collided
	// used in states to check if object has collided
	// so the state can be changed
	virtual bool getCollided() = 0;
	virtual void setCollided(bool ifCollided) = 0;

	// get for items draw colours
	virtual GLfloat getRed() = 0;
	virtual GLfloat getGreen() = 0;
	virtual GLfloat getBlue() = 0;

	// items effect on player
	virtual void itemEffect(Player * player) = 0;

	virtual ~AbstractItem() {}

protected:
	GLfloat red, green ,blue;

	int hitPoints;
	int strength;
	int speed;

	float itemXPos, itemYPos;
	float itemXSize, itemYSize;

	bool collided;
};

#endif