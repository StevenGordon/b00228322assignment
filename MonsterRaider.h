#ifndef MONSTERRAIDER_H
#define MONSTERRAIDER_H

#include "Monster.h"

class MonsterRaider : public Monster
{
public:
	MonsterRaider(void);  
	~MonsterRaider(void);

	void init();
	void draw(SDL_Window * window);
	bool drop(Player * player);
};

#endif