#include "StateCredits.h"
#include <iostream>

#include "Game.h"


StateCredits::StateCredits(Game * gameRefs)
{
	gameRef = gameRefs;
}

StateCredits::~StateCredits(void)
{
}

// sets the clock to now and the end clock
// to now + delay
// (delay * CLOCKS_PER_SEC) because time 
// is measured in milliseconds
void StateCredits::init(Game &context)
{
	now = clock();

	delay = 30;

	end = now + (delay * CLOCKS_PER_SEC);

	creditsLabel = new Label();
	creditsXPos = -0.85f; creditsYPos = 0.7f;
}

void StateCredits::draw(SDL_Window * window, Game &context)
{
	// sets label colour
	SDL_Color creditsColour = { 255, 255, 255 };

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window

	// draw title credits
	creditsLabel->displayString(creditsXPos, creditsYPos, "Credits", creditsColour, 0.01f);

	SDL_GL_SwapWindow(window); // swap buffers

}

// switches state to main menu state by pressing the space or return key
void StateCredits::handleSDLEvent(SDL_Event const &sdlEvent, Game &context)
{
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		//std::cout << "Scancode: " << sdlEvent.key.keysym.scancode  ;
        //std::cout <<  ", Name: " << SDL_GetKeyName( sdlEvent.key.keysym.sym ) << std::endl;
		switch( sdlEvent.key.keysym.sym )
		{
		case SDLK_RETURN:
		case SDLK_SPACE:
			context.setState(context.getMainMenuState());
		default:
			break;
		}
	}
}

// if the now clock is equal to or greater
// than the end time this switches
// to state to the main menu
void StateCredits::update(Game &context)
{
	now = clock();

	if (end <= now)
	{
		context.setState(context.getMainMenuState());
    }
}