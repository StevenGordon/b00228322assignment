#include <iostream>
#include "Label.h"
#include "Game.h"

Label::Label()
{
	height = 0;
	width = 0;
}

// textToTexture
// A static member function: does not need an instance of the label class
GLuint Label::textToTexture(const char * str, SDL_Color colour)
{
	// set up TrueType / SDL_ttf
	if (TTF_Init()== -1)
		Game::exitFatalError("TTF failed to initialise.");

	TTF_Font *textFont;
	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	SDL_Surface *stringImage;
	stringImage = TTF_RenderText_Blended(textFont,str,colour);

	if (stringImage == NULL)
		Game::exitFatalError("String surface not created.");

	GLuint colours = stringImage->format->BytesPerPixel;

	GLuint format, internalFormat;
	if (colours == 4) {   // alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGBA;
	    else
		    format = GL_BGRA;
	} else {             // no alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGB;
	    else
		    format = GL_BGR;
	}
	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;

	glGenTextures(1, &texID);
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, stringImage->w, stringImage->h, 0,
                    format, GL_UNSIGNED_BYTE, stringImage->pixels);
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);

	width = stringImage->w;
	height = stringImage->h;

	// SDL surface was used to generate the texture but is no longer
	// required. Release it to free memory
	SDL_FreeSurface(stringImage);
	TTF_CloseFont(textFont);

	return texID;
}

// This function assumes that strings are dynamic:
// creating and deleting textures for the string
// Strings that remain throughout the game should only be generated once
// or should be generated at compile time and loaded as fixed textures
// Generating textures during init at run time can make it easier to change
// text, while using artist generated textures can allow for a much more
// professional quality finish on the graphics
void Label::displayString(float x, float y, const char * str, SDL_Color colour, GLfloat scale)
{
	texID = textToTexture(str, colour);

	// Draw texture here
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	
	draw(x, y, scale);

	glDisable(GL_TEXTURE_2D);

	glDeleteTextures(1, &texID);
}

// Draw a label with text scaled by scale factor
void Label::draw(GLfloat x, GLfloat y, GLfloat scale)
{
	// Draw texture here
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	
    glBegin(GL_QUADS);
	  glTexCoord2d(0,1); // Texture has origin at top not bottom
      glVertex2f (x,y); // first corner
	  glTexCoord2d(1,1);
	  glVertex2f (x+scale*width, y); // second corner
	  glTexCoord2d(1,0);
      glVertex2f (x+scale*width, y+scale*height); // third corner
	  glTexCoord2d(0,0);
      glVertex2f (x, y+scale*height); // fourth corner
    glEnd();

	glDisable(GL_TEXTURE_2D);
}