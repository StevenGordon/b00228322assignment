#ifndef STATESPLASHSCREEN_H
#define STATESPLASHSCREEN_H

#include "GameState.h"
#include "Label.h"
#include <ctime>

class StateSplashScreen : public GameState
{
public:
	StateSplashScreen(Game * gameRefs);
	~StateSplashScreen(void);

	void init(Game &context);

	void draw(SDL_Window * window, Game &context);

	void handleSDLEvent(SDL_Event const &sdlEvent, Game &context);
	void update(Game &context);

private:
	int delay;

	float gameNameXPos, gameNameYPos;

	SDL_Color TitleColour;

	clock_t now;
	clock_t end;

	Game * gameRef;
	Label * companyNameLabel;
};

#endif