#ifndef MONSTERFODDER_H
#define MONSTERFODDER_H

#include "Monster.h"

class MonsterFodder : public Monster
{
public:
	MonsterFodder(void);
	~MonsterFodder(void);

	void init();
	void draw(SDL_Window * window);
	bool drop(Player * player);
};

#endif