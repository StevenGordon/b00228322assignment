#include "StateCombat.h"
#include <iostream>

// stringstream and string
#include <sstream>
#include <string>

// While a declaration of class Game has been provided in
// gamestate.h, this was only a forward declaration
// because we need to work with the implementation now
// we need to include game.h here
#include "Game.h"

StateCombat::StateCombat(Game * gameRefs)
{
	gameRef = gameRefs;
}


StateCombat::~StateCombat(void)
{
}

void StateCombat::init(Game &context)
{	
	fightCompleted = false;

	playerInitialHitPoints = gameRef->getPlayer()->getPlayerCurrentHitPoints();

	playerXPos = -0.6f; playerYPos = 0.0f;

	monsterXPos = 0.3f;	monsterYPos = 0.0f;

	orderOfAttack = 0;

	setCoords();
	
	playerStatsLabel = new Label();

	monsterStatsLabel = new Label();
}

// Checks each monster if it has collided with the Player.
// Then return that monster.
Monster * StateCombat::getMonster()
{
	if(gameRef->getFodder1() != NULL &&  gameRef->getFodder1()->getCollided())
		return gameRef->getFodder1();
	if(gameRef->getFodder2() != NULL &&  gameRef->getFodder2()->getCollided())
		return gameRef->getFodder2();
	if(gameRef->getFodder3() != NULL &&  gameRef->getFodder3()->getCollided())
		return gameRef->getFodder3();
	if(gameRef->getFodder4() != NULL &&  gameRef->getFodder4()->getCollided())
		return gameRef->getFodder4();
	if(gameRef->getFodder5() != NULL &&  gameRef->getFodder5()->getCollided())
		return gameRef->getFodder5();

	if(gameRef->getBrute1() != NULL &&  gameRef->getBrute1()->getCollided())
		return gameRef->getBrute1();
	if(gameRef->getBrute2() != NULL &&  gameRef->getBrute2()->getCollided())
		return gameRef->getBrute2();
	if(gameRef->getBrute3() != NULL &&  gameRef->getBrute3()->getCollided())
		return gameRef->getBrute3();

	if(gameRef->getRaider1() != NULL &&  gameRef->getRaider1()->getCollided())
		return gameRef->getRaider1();

	return NULL;
}

// sets temp coords to the players coords 
// and then sets the players coords to
// the combat coords
void StateCombat::setCoords()
{
	tempPlayerX = gameRef->getPlayer()->getPlayerXPos();
	tempPlayerY = gameRef->getPlayer()->getPlayerYPos();
	gameRef->getPlayer()->setPlayerXPos(playerXPos);
	gameRef->getPlayer()->setPlayerYPos(playerYPos);

	tempMonsterX = getMonster()->getMonsterXPos();
	tempMonsterY = getMonster()->getMonsterYPos();
	getMonster()->setMonsterXPos(monsterXPos);
	getMonster()->setMonsterYPos(monsterYPos);
}

void StateCombat::draw(SDL_Window * window, Game &context)
{
	// set label colour
	SDL_Color GameColour = { 255, 255, 0 };

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window

	gameRef->getPlayer()->draw(window);
	playerStatsLabel->displayString(gameRef->getPlayer()->getPlayerXPos() + (gameRef->getPlayer()->getPlayerXSize()/2.0f),
		gameRef->getPlayer()->getPlayerYPos()+gameRef->getPlayer()->getPlayerYSize(), "Player", GameColour, 0.002f);

	getMonster()->draw(window);

	// Print out the players and monsters stats information
	// player stats
	std::stringstream strStreamPlayerHealth;
	strStreamPlayerHealth << "HP: " << gameRef->getPlayer()->getPlayerCurrentHitPoints();
	strStreamPlayerHealth << " / " << gameRef->getPlayer()->getPlayerTotalHitPoints();
	playerStatsLabel->displayString(-0.6f, -0.1f, strStreamPlayerHealth.str().c_str(), GameColour, 0.002f);

	std::stringstream strStreamPlayerStrength;
	strStreamPlayerStrength << "STR: " << gameRef->getPlayer()->getPlayerStrength();
	playerStatsLabel->displayString(-0.6f, -0.2f, strStreamPlayerStrength.str().c_str(), GameColour, 0.002f);

	std::stringstream strStreamPlayerSpeed;
	strStreamPlayerSpeed << "SPD: " << gameRef->getPlayer()->getPlayerSpeed();
	playerStatsLabel->displayString(-0.6f, -0.3f, strStreamPlayerSpeed.str().c_str(), GameColour, 0.002f);


	// monster stats
	std::stringstream strStreamMonsterHealth;
	strStreamMonsterHealth << "HP: " <<  getMonster()->getMonsterHitPoints();
	monsterStatsLabel->displayString(0.3f, -0.1f, strStreamMonsterHealth.str().c_str(), GameColour, 0.002f);

	std::stringstream strStreamMonsterStrength;
	strStreamMonsterStrength << "STR: " <<  getMonster()->getMonsterStrength();
	monsterStatsLabel->displayString(0.3f, -0.2f, strStreamMonsterStrength.str().c_str(), GameColour, 0.002f);

	std::stringstream strStreamMonsterSpeed;
	strStreamMonsterSpeed << "SPD: " <<  getMonster()->getMonsterSpeed();
	monsterStatsLabel->displayString(0.3f, -0.3f, strStreamMonsterSpeed.str().c_str(), GameColour, 0.002f);

	SDL_GL_SwapWindow(window); // swap buffers
}

// switches state to main menu state by pressing the space or return key
// if combat has been resolved
void StateCombat::handleSDLEvent(SDL_Event const &sdlEvent, Game &context)
{
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		//std::cout << "Scancode: " << sdlEvent.key.keysym.scancode  ;
		//std::cout <<  ", Name: " << SDL_GetKeyName( sdlEvent.key.keysym.sym ) << std::endl;
		switch( sdlEvent.key.keysym.sym )
		{
		case SDLK_RETURN:
		case SDLK_SPACE:
			combat(context);
			break;
		default:
			break;
		}
	}
}

void StateCombat::update(Game &context) { }

// checks who attacks first by comparing speeds
void StateCombat::speedCheck()
{
	// Speed check
	if(gameRef->getPlayer()->getPlayerSpeed() > getMonster()->getMonsterSpeed())
		orderOfAttack = 1;	// player attacks first
	if(gameRef->getPlayer()->getPlayerSpeed() < getMonster()->getMonsterSpeed())
		orderOfAttack = 2;	// monster attacks first
	if(gameRef->getPlayer()->getPlayerSpeed() == getMonster()->getMonsterSpeed())
		orderOfAttack = (int)( 1 + (float)(rand() %2) );
}

// dependant on teh order of attack
// the player and monster take turns damaging each other
// until either the player HP is less than or equal to 0
// or the monster HP is less than or equal to 0
// then sets the boolean fightCompleted to TRUE
void StateCombat::fight()
{
	if(fightCompleted == false)
	{
		if (orderOfAttack == 1)
		{
			while( (gameRef->getPlayer()->getPlayerCurrentHitPoints() > 0) && (getMonster()->getMonsterHitPoints() > 0) )
			{
				getMonster()->setMonsterHitPoints(gameRef->getPlayer()->getDamage());
				std::cout << "Monster HP " << getMonster()->getMonsterHitPoints() << std::endl;
				gameRef->getPlayer()->setDamage(getMonster()->getDamage());
				std::cout << "Player HP " << gameRef->getPlayer()->getPlayerCurrentHitPoints() << std::endl;
			}
		}

		if (orderOfAttack == 2)
		{
			while( (gameRef->getPlayer()->getPlayerCurrentHitPoints() > 0) && (getMonster()->getMonsterHitPoints() > 0) )
			{
				gameRef->getPlayer()->setDamage(getMonster()->getDamage());
				std::cout << "Player HP " << gameRef->getPlayer()->getPlayerCurrentHitPoints() << std::endl;
				getMonster()->setMonsterHitPoints(gameRef->getPlayer()->getDamage());
				std::cout << "Monster HP " << getMonster()->getMonsterHitPoints() << std::endl;
			}
		}

		fightCompleted = true;
	}
}

// if the monster drops an item
// the monster drops a randomly selected
// item which effects the player
void StateCombat::itemDrop(Game &context)
{
	if( getMonster()->drop(gameRef->getPlayer()) )
	{
		// creates a number between 0 - 99 (100 possible values)
		int itemCreated = (float)( rand() %100 );
		std::cout << itemCreated << std::endl;
		// health packs have a 60% drop rate
		// so 0 - 59 is 60 in 100 chance so 60%
		if(itemCreated <= 59)
		{
			if(context.getDroppedItem() == NULL)
			{
				context.setDroppedItem(  new ItemHealthPack(new Item("Health Pack", 1, 0, 0, 1.0f, 0.0f, 0.0f)) );
				// use item effect
				context.getDroppedItem()->itemEffect(gameRef->getPlayer());
				// delete item
				delete context.getDroppedItem();
				context.setDroppedItem(NULL);	
			}
		}
		// combat packs have a 20% drop rate
		// so 60 - 79 is 20 in 100 chance so 20%
		else if(itemCreated >= 60 && itemCreated <= 79)
		{
			if(context.getDroppedItem() == NULL)
			{
				context.setDroppedItem(  new ItemCombatPack(new Item("Combat Pack", 0, 2, 0, 0.0f, 0.0f, 1.0f)) );
				// use item effect
				context.getDroppedItem()->itemEffect(gameRef->getPlayer());
				// delete item
				delete context.getDroppedItem();
				context.setDroppedItem(NULL);	
			}
		}
		// health packs have a 20% drop rate
		// so 80 - 99 is 20 in 100 chance so 20%
		else if(itemCreated >= 80 && itemCreated <= 99)
		{
			if(context.getDroppedItem() == NULL)
			{
				context.setDroppedItem( new ItemStimulant(new Item("Combat Pack", 0, 0, 1, 0.0f, 1.0f, 0.0f)) );
				// use item effect
				context.getDroppedItem()->itemEffect(gameRef->getPlayer());
				// delete item
				delete context.getDroppedItem();
				context.setDroppedItem(NULL);	
			}
		}

	}
}

// checks which was defeated first player of monster;
// depending on order of attack;
void StateCombat::checkDefeated(Game &context)
{
	if (orderOfAttack == 1)
	{
		if( getMonster()->getMonsterHitPoints() <= 0 )
		{
			endFight();
			itemDrop(context);
			getMonster()->resetCollided();
			context.setState(context.getMapState());
		}
		else if(gameRef->getPlayer()->getPlayerCurrentHitPoints() <= 0)
		{
			context.setState(context.getGameOverState());
		} 
	}

	if (orderOfAttack == 2)
	{
		if(gameRef->getPlayer()->getPlayerCurrentHitPoints() <= 0)
		{
			context.setState(context.getGameOverState());
		}
		else if( getMonster()->getMonsterHitPoints() <= 0 )
		{
			endFight();
			itemDrop(context);
			getMonster()->resetCollided();
			context.setState(context.getMapState());
		}
	}
}

// Sets the players coords back to it's original coords
// from the map state collision and "heals" the player
// by half the total damage recived
void StateCombat::endFight()
{
	int damage;
	int heal;

	gameRef->getPlayer()->setPlayerXPos(tempPlayerX);
	gameRef->getPlayer()->setPlayerYPos(tempPlayerY);

	damage = playerInitialHitPoints - gameRef->getPlayer()->getPlayerCurrentHitPoints();
	std::cout << damage << std::endl;
	heal = (damage) / 2;
	std::cout << heal << std::endl;

	gameRef->getPlayer()->setheal(heal);
	std::cout << gameRef->getPlayer()->getPlayerCurrentHitPoints() << std::endl;
}

void StateCombat::combat(Game &context)
{
	if(!fightCompleted)
	{
		speedCheck();
		fight();
	}
	else if(fightCompleted)
	{
		checkDefeated(context);
	}
}

