#ifndef MONSTERBRUTE_H
#define MONSTERBRUTE_H

#include "Monster.h"

class MonsterBrute : public Monster
{
public:
	MonsterBrute(void);
	~MonsterBrute(void);

	void init();
	void draw(SDL_Window * window);
	bool drop(Player * player);
};

#endif