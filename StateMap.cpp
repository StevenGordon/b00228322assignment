#include "StateMap.h"
#include <iostream>
// stringstream and string
#include <sstream>
#include <string>

// While a declaration of class Game has been provided in
// gamestate.h, this was only a forward declaration
// because we need to work with the implementation now
// we need to include game.h here
#include "Game.h"

StateMap::StateMap(Game * gameRefs)
{
	gameRef = gameRefs;
}

StateMap::~StateMap(void)
{
}

// creates and initializes objects the state requires 
void StateMap::init(Game &context)
{	
	playerStatesLabel = new Label();

	std::srand( std::time(NULL) );

	gameRef->getPlayer()->init();

	/* if monsters do not equal NULL set monsters variables */
	// set fodder variables
	if(context.getFodder1() != NULL)
		gameRef->getFodder1()->init();
	if(context.getFodder2() != NULL)
		gameRef->getFodder2()->init();
	if(context.getFodder3() != NULL)
		gameRef->getFodder3()->init();
	if(context.getFodder4() != NULL)
		gameRef->getFodder4()->init();
	if(context.getFodder5() != NULL)
		gameRef->getFodder5()->init();
	// set brute variables
	if(context.getBrute1() != NULL)
		gameRef->getBrute1()->init();
	if(context.getBrute2() != NULL)
		gameRef->getBrute2()->init();
	if(context.getBrute3() != NULL)
		gameRef->getBrute3()->init();
	// set raider variables
	if(context.getRaider1() != NULL)
		gameRef->getRaider1()->init();

	/* if items do not equal NULL set items variables */
	// set health pack variables
	if(context.getHealthPack1() != NULL)
		context.getHealthPack1()->init();
	if(context.getHealthPack2() != NULL)
		context.getHealthPack2()->init();
	// set combat pack variables
	if(context.getCombatPack1() != NULL)
		context.getCombatPack1()->init();
	if(context.getCombatPack2() != NULL)
		context.getCombatPack2()->init();
	// set stimulant variables
	if(context.getStimulant1() != NULL)
		context.getStimulant1()->init();
}

void StateMap::draw(SDL_Window * window, Game &context)
{
	SDL_Color GameColour = { 255, 255, 0 };

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window

	//draw player and player label
	gameRef->getPlayer()->draw(window);

	//draw health pack items
	if(context.getHealthPack1() != NULL)
		context.getHealthPack1()->draw();
	if(context.getHealthPack2() != NULL)
		context.getHealthPack2()->draw();

	//draw combat pack items
	if(context.getCombatPack1() != NULL)
		context.getCombatPack1()->draw();
	if(context.getCombatPack2() != NULL)
		context.getCombatPack2()->draw();

	//draw stimulant items
	if(context.getStimulant1() != NULL)
		context.getStimulant1()->draw();

	//draw fodder monsters
	if(context.getFodder1() != NULL)
		gameRef->getFodder1()->draw(window);
	if(context.getFodder2() != NULL)
		gameRef->getFodder2()->draw(window);
	if(context.getFodder3() != NULL)
		gameRef->getFodder3()->draw(window);
	if(context.getFodder4() != NULL)
		gameRef->getFodder4()->draw(window);
	if(context.getFodder5() != NULL)
		gameRef->getFodder5()->draw(window);

	//draw brute monsters
	if(context.getBrute1() != NULL)
		gameRef->getBrute1()->draw(window);
	if(context.getBrute2() != NULL)
		gameRef->getBrute2()->draw(window);
	if(context.getBrute3() != NULL)
		gameRef->getBrute3()->draw(window);

	//draw raider monsters
	if(context.getRaider1() != NULL)
		gameRef->getRaider1()->draw(window);

	// Print out the score and frame time information
	std::stringstream strStream;
	strStream << "     $:" << gameRef->getPlayer()->getPlayerDollars();
	strStream << "     HP: " << gameRef->getPlayer()->getPlayerCurrentHitPoints();
	strStream << " / " << gameRef->getPlayer()->getPlayerTotalHitPoints();
	strStream << "  STR: " << gameRef->getPlayer()->getPlayerStrength();
	strStream << "  SPD: " << gameRef->getPlayer()->getPlayerSpeed();

	playerStatesLabel->displayString(-0.9f,-0.9f, strStream.str().c_str(), GameColour, 0.002f);

	SDL_GL_SwapWindow(window); // swap buffers
}

void StateMap::itemCollision(Game &context)
{
	itemHealthCollision(context);
	itemCombatCollision(context);
	itemStimCollision(context);
}

void StateMap::itemHealthCollision(Game &context)
{
	if(gameRef->getHealthPack1() != NULL)
		gameRef->getHealthPack1()->collision(gameRef->getPlayer());

	if(context.getHealthPack1() != NULL && gameRef->getHealthPack1()->getCollided())
	{
		context.getHealthPack1()->itemEffect(gameRef->getPlayer());

		delete context.getHealthPack1();
		context.setHealthPack1(NULL);
	}

	if(gameRef->getHealthPack2() != NULL)
		gameRef->getHealthPack2()->collision(gameRef->getPlayer());

	if(context.getHealthPack2() != NULL && gameRef->getHealthPack2()->getCollided())
	{
		context.getHealthPack2()->itemEffect(gameRef->getPlayer());

		delete context.getHealthPack2();
		context.setHealthPack2(NULL);
	}

	if(gameRef->getDroppedItem() != NULL)
		gameRef->getDroppedItem()->collision(gameRef->getPlayer());

	if(context.getDroppedItem() != NULL && gameRef->getDroppedItem()->getCollided())
	{
		context.getDroppedItem()->itemEffect(gameRef->getPlayer());

		delete context.getDroppedItem();
		context.setDroppedItem(NULL);
	}
}

void StateMap::itemCombatCollision(Game &context)
{
	if(gameRef->getCombatPack1() != NULL)
		gameRef->getCombatPack1()->collision(gameRef->getPlayer());

	if(context.getCombatPack1() != NULL && gameRef->getCombatPack1()->getCollided())
	{
		context.getCombatPack1()->itemEffect(gameRef->getPlayer());

		delete context.getCombatPack1();
		context.setCombatPack1(NULL);
	}

	if(gameRef->getCombatPack2() != NULL)
		gameRef->getCombatPack2()->collision(gameRef->getPlayer());

	if(context.getCombatPack2() != NULL && gameRef->getCombatPack2()->getCollided())
	{
		context.getCombatPack2()->itemEffect(gameRef->getPlayer());

		delete context.getCombatPack2();
		context.setCombatPack2(NULL);
	}

	if(gameRef->getDroppedItem() != NULL)
		gameRef->getDroppedItem()->collision(gameRef->getPlayer());

	if(context.getDroppedItem() != NULL && gameRef->getDroppedItem()->getCollided())
	{
		context.getDroppedItem()->itemEffect(gameRef->getPlayer());

		delete context.getDroppedItem();
		context.setDroppedItem(NULL);
	}
}

void StateMap::itemStimCollision(Game &context)
{
	if(gameRef->getStimulant1() != NULL)
		gameRef->getStimulant1()->collision(gameRef->getPlayer());

	if(context.getStimulant1() != NULL && gameRef->getStimulant1()->getCollided())
	{
		context.getStimulant1()->itemEffect(gameRef->getPlayer());
		
		delete context.getStimulant1();
		context.setStimulant1(NULL);
	}

	if(gameRef->getDroppedItem() != NULL)
		gameRef->getDroppedItem()->collision(gameRef->getPlayer());

	if(context.getDroppedItem() != NULL && gameRef->getDroppedItem()->getCollided())
	{
		context.getDroppedItem()->itemEffect(gameRef->getPlayer());

		delete context.getDroppedItem();
		context.setDroppedItem(NULL);
	}
}

void StateMap::collision(Game &context)
{
	itemCollision(context);	

	if(context.getFodder1() != NULL)
		gameRef->getFodder1()->collision(gameRef->getPlayer());
	if(context.getFodder2() != NULL)
		gameRef->getFodder2()->collision(gameRef->getPlayer());
	if(context.getFodder3() != NULL)
		gameRef->getFodder3()->collision(gameRef->getPlayer());
	if(context.getFodder4() != NULL)
		gameRef->getFodder4()->collision(gameRef->getPlayer());
	if(context.getFodder5() != NULL)
		gameRef->getFodder5()->collision(gameRef->getPlayer());

	if(context.getBrute1() != NULL)
		gameRef->getBrute1()->collision(gameRef->getPlayer());
	if(context.getBrute2() != NULL)
		gameRef->getBrute2()->collision(gameRef->getPlayer());
	if(context.getBrute3() != NULL)
		gameRef->getBrute3()->collision(gameRef->getPlayer());

	if(context.getRaider1() != NULL)
		gameRef->getRaider1()->collision(gameRef->getPlayer());

	if(context.getFodder1() != NULL && gameRef->getFodder1()->getCollided())
		context.setState(context.getCombatState());
	if(context.getFodder2() != NULL && gameRef->getFodder2()->getCollided())
		context.setState(context.getCombatState());
	if(context.getFodder3() != NULL && gameRef->getFodder3()->getCollided())
		context.setState(context.getCombatState());
	if(context.getFodder4() != NULL && gameRef->getFodder4()->getCollided())
		context.setState(context.getCombatState());
	if(context.getFodder5() != NULL && gameRef->getFodder5()->getCollided())
		context.setState(context.getCombatState());

	if(context.getBrute1() != NULL && gameRef->getBrute1()->getCollided())
		context.setState(context.getCombatState());
	if(context.getBrute2() != NULL && gameRef->getBrute2()->getCollided())
		context.setState(context.getCombatState());
	if(context.getBrute3() != NULL && gameRef->getBrute3()->getCollided())
		context.setState(context.getCombatState());

	if(context.getRaider1() != NULL && gameRef->getRaider1()->getCollided())
		context.setState(context.getCombatState());
}

void StateMap::handleSDLEvent(SDL_Event const &sdlEvent, Game &context)
{
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		//std::cout << "Scancode: " << sdlEvent.key.keysym.scancode  ;
		//std::cout <<  ", Name: " << SDL_GetKeyName( sdlEvent.key.keysym.sym ) << std::endl;
		switch( sdlEvent.key.keysym.sym )
		{
		case SDLK_UP:
		case 'w': case 'W': 
			gameRef->getPlayer()->movePlayerYPos(0.05f);
			break;
		case SDLK_DOWN:
		case 's': case 'S':
			gameRef->getPlayer()->movePlayerYPos(-0.05f);
			break;
		case SDLK_LEFT:
		case 'a': case 'A': 
			gameRef->getPlayer()->movePlayerXPos(-0.05f);
			break;
		case SDLK_RIGHT:
		case 'd': case 'D':
			gameRef->getPlayer()->movePlayerXPos(0.05f);
			break;
		case SDLK_ESCAPE:
			context.setState(context.getMainMenuState());
			break;
		default:
			break;
		}
	}
}

void StateMap::resetMonsters(Game &context)
{
	if( context.getFodder1() != NULL &&  gameRef->getFodder1()->getMonsterHitPoints() <= 0 )
	{
		delete gameRef->getFodder1();
		gameRef->setFodder1(NULL);
		deadMonsterCounter ++;
	}
	if( context.getFodder2() != NULL && gameRef->getFodder2()->getMonsterHitPoints() <= 0 )
	{
		delete gameRef->getFodder2();
		gameRef->setFodder2(NULL);
		deadMonsterCounter ++;
	}
	if( context.getFodder3() != NULL && gameRef->getFodder3()->getMonsterHitPoints() <= 0 )
	{
		delete gameRef->getFodder3();
		gameRef->setFodder3(NULL);
		deadMonsterCounter ++;
	}
	if( context.getFodder4() != NULL && gameRef->getFodder4()->getMonsterHitPoints() <= 0 )
	{
		delete gameRef->getFodder4();
		gameRef->setFodder4(NULL);
		deadMonsterCounter ++;
	}
	if( context.getFodder5() != NULL && gameRef->getFodder5()->getMonsterHitPoints() <= 0 )
	{
		delete gameRef->getFodder5();
		gameRef->setFodder5(NULL);
		deadMonsterCounter ++;
	}


	if( context.getBrute1() != NULL && gameRef->getBrute1()->getMonsterHitPoints() <= 0 )
	{
		delete gameRef->getBrute1();
		gameRef->setBrute1(NULL);
		deadMonsterCounter ++;
	}
	if( context.getBrute2() != NULL && gameRef->getBrute2()->getMonsterHitPoints() <= 0 )
	{
		delete gameRef->getBrute2();
		gameRef->setBrute2(NULL);
		deadMonsterCounter ++;
	}
	if( context.getBrute3() != NULL && gameRef->getBrute3()->getMonsterHitPoints() <= 0 )
	{
		delete gameRef->getBrute3();
		gameRef->setBrute3(NULL);
		deadMonsterCounter ++;
	}


	if( context.getRaider1() != NULL && gameRef->getRaider1()->getMonsterHitPoints() <= 0 )
	{
		delete gameRef->getRaider1();
		gameRef->setRaider1(NULL);
		deadMonsterCounter ++;
	}
}

void StateMap::monsterdead(Game &context)
{
	deadMonsterCounter = 0;
	resetMonsters(context);
	if(deadMonsterCounter == 9)
		context.setState(context.getGameOverState());
}

void StateMap::update(Game &context)
{
	collision(context);
	monsterdead(context);
}