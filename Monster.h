#ifndef MONSTER_H
#define MONSTER_H

#include <GL/glew.h>
#include <SDL.h>
#include <SDL_ttf.h>
#include <cstdlib>
#include <ctime>

#include "Label.h"

#include "Player.h"

class Monster
{
public:
	friend class Game;

	virtual ~Monster(void) { return; }

	virtual void init() = 0;
	virtual void draw(SDL_Window * window) = 0;
	virtual bool drop(Player * player) = 0;

	float getMonsterXPos() { return monsterXPos; }
	float getMonsterYPos() { return monsterYPos; }
	void setMonsterXPos(float newXPos) { monsterXPos = newXPos; }
	void setMonsterYPos(float newYPos) { monsterYPos = newYPos; }

	float getMonsterXSize() { return monsterXSize; }
	float getMonsterYSize() { return monsterYSize; }

	int getMonsterHitPoints() { return monsterHitPoints; }
	void setMonsterHitPoints(int damage) { monsterHitPoints -= damage; }

	int getMonsterStrength() { return monsterStrength; }
	int getMonsterSpeed() { return monsterSpeed; }

	bool getCollided() { return hasCollided; }
	void resetCollided() { hasCollided = false; }

	int getDamage()
	{
		srand(time(NULL));
		return (int)( 2 + (float)( rand() %(monsterStrength-2) ) );
	}

	void collision(Player * player)
	{
		if ( (monsterXPos >= player->getPlayerXPos())
			&& (monsterXPos+monsterXSize <= player->getPlayerXPos()+player->getPlayerXSize())	// cursor surrounds target in x

			&& (monsterYPos >= player->getPlayerYPos())
			&& (monsterYPos+monsterYSize <= player->getPlayerYPos()+player->getPlayerXSize()) ) // cursor surrounds target in y
		{
			hasCollided = true;
		}
	}

	void displayLabel(float x, float y, const char * str, SDL_Color colour, GLfloat scale)
	{
		monsterLabel = new Label();
		monsterLabel->displayString(x, y, str, colour, scale);
	}

protected:
	bool hasCollided;

	int monsterHitPoints, monsterStrength, monsterSpeed;
	int monsterDollars, monsterItemChance;

	float monsterXPos, monsterYPos, monsterXSize, monsterYSize;

	SDL_Color Color;

	Label * monsterLabel;
};

#endif