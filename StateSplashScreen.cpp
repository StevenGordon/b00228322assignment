#include "StateSplashScreen.h"
#include <iostream>

#include "Game.h"

StateSplashScreen::StateSplashScreen(Game * gameRefs)
{
	gameRef = gameRefs;
}

StateSplashScreen::~StateSplashScreen(void)
{
}

// sets the clock to now and the end clock
// to now + delay
// (delay * CLOCKS_PER_SEC) because time 
// is measured in milliseconds
void StateSplashScreen::init(Game &context)
{
	now = clock();
	delay = 3;

	end = now + (delay * CLOCKS_PER_SEC);

	companyNameLabel = new Label();
	gameNameXPos = -0.85f; gameNameYPos = 0.0f;
}

void StateSplashScreen::draw(SDL_Window * window, Game &context)
{
	// sets label colour
	SDL_Color splashScreenColour = { 100, 70, 255 };

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window

	// draw company name
	companyNameLabel->displayString(gameNameXPos, gameNameYPos, "Company Name", splashScreenColour, 0.01f);

	SDL_GL_SwapWindow(window); // swap buffers
}

// switches state to main menu state by pressing the space or return key
void StateSplashScreen::handleSDLEvent(SDL_Event const &sdlEvent, Game &context)
{
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		//std::cout << "Scancode: " << sdlEvent.key.keysym.scancode  ;
        //std::cout <<  ", Name: " << SDL_GetKeyName( sdlEvent.key.keysym.sym ) << std::endl;
		switch( sdlEvent.key.keysym.sym )
		{
		case SDLK_RETURN:
		case SDLK_SPACE:
			context.setState(context.getMainMenuState());
		default:
			break;
		}
	}
}

// if the now clock is equal to or greater
// than the end time this switches
// to state to the main menu
void StateSplashScreen::update(Game &context)
{
	now = clock();

	if (end <= now)
	{
		context.setState(context.getMainMenuState());
    }
}