#include <iostream>
#include "Game.h"

// SDL projects do not automatically work with the console window. 
// On windows with visual studio, the following line is required to use console output
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

int main(int argc, char *argv[])
{
	Game *newGame = new Game();

	newGame->init();
	newGame->run();

	delete newGame;
    return 0;
}